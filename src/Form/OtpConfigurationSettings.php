<?php

namespace Drupal\otp_registration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure otp_registration for this site.
 */
class OtpConfigurationSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'otp_registration.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'otp_registration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('otp_registration.settings');
    $form['otp_length'] = [
      '#type' => 'textfield',
      '#title' => t('OTP length'),
      '#default_value' => $config->get('otp_length'),
    ];
    $form['otp_validity'] = [
      '#type' => 'textfield',
      '#title' => t('OTP Validation Time(in mins)'),
      '#default_value' => $config->get('otp_validity'),
    ];
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('otp_length'))) {
      $form_state->setErrorByName('otp_length', t('Please enter a valid length'));
    }
    if (!is_numeric($form_state->getValue('otp_validity'))) {
      $form_state->setErrorByName('otp_validity', t('Please enter a valid OTP validation time'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config('otp_registration.settings')
      // Set the submitted configuration settings.
      ->set('otp_length', $form_state->getValue('otp_length'))
      ->set('otp_validity', $form_state->getValue('otp_validity'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
