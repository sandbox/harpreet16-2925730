<?php

namespace Drupal\otp_registration;

/**
 * Contains \Drupal\otp_registration\Otpservices.
 */
class Otpservices {

  /**
   * Function for OTP generation.
   */
  public function otpRegistrationGetOtp($length, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789') {
    $myConfig = \Drupal::config('otp_registration.settings');
    $length = $myConfig->get('otp_length');
    $chars_length = (strlen($chars) - 1);
    $string = $chars{rand(0, $chars_length)};
    for ($i = 1; $i < $length; $i = strlen($string)) {
      $r = $chars{rand(0, $chars_length)};
      if ($r != $string{$i - 1}) $string .= $r;
    }
    return $string;
  }

  /**
   * Save otp temporarily.
   */
  public function otpRegistrationSaveOtp($mail, $otp) {
    // db_query to obtain otp from mail.
    $rows = $this->getOtpFromMail($mail);
    $rows->allowRowCount = TRUE;
    // If entry for the user exists, otp and time entry would be updated.
    if ($rows->rowCount() > 0) {
      $query = db_update('otp_registration')
        ->fields([
          'otp' => $otp,
          'time_entry' => strtotime("now"),
        ])
        ->condition('mail_id', $mail, '=')
        ->execute();
    }
    // New entry for a fresh user attempting to generate otp for first time.
    else {
      db_insert('otp_registration')->fields(
        [
          'mail_id' => $mail,
          'otp' => $otp,
          'time_entry' => strtotime("now"),
        ]
      )
        ->execute();
    }
  }

  /**
   * Db_query to retrieve otp from db_table.
   */
  public function getOtpFromMail($mail) {
    $query = db_select('otp_registration', 'otp')
      ->fields('otp')
      ->condition('otp.mail_id', $mail)
      ->execute();
    return $query;
  }

}
